import { setUpTestDatabase } from "./setUp";
import request from 'supertest';
import { server } from "../src/server";



describe('article controller', () => {

    setUpTestDatabase();

    it('should return the article on get', async () => {
        const response = await request(server)
        .get('/api/article')
        .expect(200);

        expect(response.body).toContainEqual({
            id: expect.any(Number),
            picture: expect.any(String),
            name: expect.any(String),
            price: expect.any(Number),
            rating: expect.any(Number),
            status: expect.any(Number),
            category: expect.anything()
        });
    })

  
})
