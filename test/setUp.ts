import { createConnection, getManager, getConnection, DeepPartial } from "typeorm";
import faker from 'faker';
import { Dog } from "../src/entity/Dog";


/**
 * Petite fonction qui met en place une base de données de test en mémoire avec
 * sqlite (et plus spécifiquement la library better-sqlite3).
 * Cette db sera recréée avant chaque test et supprimer après chaque test, il faut donc
 * faire également en sorte d'y mettre des données de test si c'est nécessaire (les fonctions
 * de fixtures servent à ça)
 */
export function setUpTestDatabase() {

    beforeEach(async () => {
        await createConnection({
            type: 'better-sqlite3',
            database: ':memory:',
            synchronize: true,
            entities: ['src/entity/*.ts'],
            dropSchema: true
        });
        await dogFixtures()
        
    });

    afterEach(async () => {
        await getConnection().close()
    })
}

export async function dogFixtures() {
    const dog:DeepPartial<Dog>[] = [];
    for (let index = 0; index < 10; index++) {
        dog.push({
            picture: faker.image.animals(),
            name: faker.name.firstName(),
            price: faker.datatype.number(),
            breed: faker.animal.dog(),
            weight: faker.datatype.number(),
            coat: faker.datatype.string(),
        })
        
    }
    await getManager().save(Dog, dog);
}


/**
 * Fonction qui fait persister des personnes pour les tests. Il faudra la lancer
 * avant chaque test et en créer d'autres pour les autres entités à tester.
 */
// async function personFixtures() {
//     await getManager().insert(Person, [
//         {name: 'Test1', age: 2},
//         {name: 'Test2', age: 4},
//         {name: 'Test3', age: 10},
//     ]);
// }
