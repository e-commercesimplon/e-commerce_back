--Table accessorie

DROP TABLE IF EXISTS accessorie;

CREATE TABLE accessorie(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    picture VARCHAR(255),
    name VARCHAR(255),
    price INT,
    rating INT,
    status INT,
    description VARCHAR(255),
    stock INT,
    categoryId INT
);

INSERT INTO accessorie (picture,name,price,rating,status,description,stock,categoryId)
VALUES ('http://cdn.shopify.com/s/files/1/0516/1176/2840/products/arriere-plan_4ab655b3-cdc7-4418-be72-c9c6eb6b561d_1200x1200.jpg?v=1611694957','Gamelle Pikachu',15,5,1,'Gamelle pour chien imprimé Pikachu',100,2),
('https://www.cdiscount.com/pdt2/0/6/9/1/700x700/mp03508069/rw/pawz-road-vetements-chien-chiot-costume-de-hallowe.jpg','Costume Pikachu',25,5,1,'Costume Pikachu pour chien',100,2);



--Table category
DROP TABLE IF EXISTS category;

CREATE TABLE category(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(255),
    status INT
);

INSERT INTO category(name, status)
VALUES ('Chiens',0),
('Accessoire',1);




--Table comment

DROP TABLE IF EXISTS comment;

CREATE TABLE comment(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    text VARCHAR(255),
    date DATE,
    status INT,
    rating INT,
    userId INT,
    accessorieId INT,
    dogId INT
);

INSERT INTO comment (text,date,status,rating,userId,accessorieId)
VALUES ('Très bonne gamelle ! Je la recommande !','2020-05-03',1,5,1,1),
('Costume très bonne qualité!','2120-08-04',1,5,1,1);




--Table dog

DROP TABLE IF EXISTS dog;

CREATE TABLE dog(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    picture VARCHAR(255),
    name VARCHAR(255),
    price INT,
    rating INT,
    breed VARCHAR (255),
    weight INT,
    coat VARCHAR(255),
    categoryId INT
);

INSERT INTO dog(picture,name,price,rating,breed,weight,coat,categoryId)
VALUES ('https://cdn.pixabay.com/photo/2016/02/19/11/53/pug-1210025_960_720.jpg','Dusty',1300,5,'Carlin',7,'court',1),
('https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg','bloup',1200,5,'Corgi',10,'court',1);

--Table order

DROP TABLE IF EXISTS order;

CREATE TABLE order(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    price INT,
    date DATE,
    status INT,
    userId INT
);



--Table Order Row
DROP TABLE IF EXISTS order_row;

CREATE TABLE order_row(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    price INT,
    quantity INT,
    orderId INT,
    accessorieId INT,
    dogId INT
);

DROP TABLE IF EXISTS user;

CREATE TABLE user(
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    email VARCHAR(255),
    password VARCHAR(255),
    name VARCHAR(255),
    firstName VARCHAR(255),
    adress VARCHAR(255),
    creditCard INT,
    role VARCHAR(255),
    lastConnection DATE,
    avatar VARCHAR(255),
    status INT
);

INSERT INTO user(email,password,name,firstName,adress,creditCard,role,lastConnection,avatar,status)
VALUES ('doggybag@yahoo.com','1234','TeamDoggy','Bag','34 Rue Antoine Primat',1234567, 'admin', '2020-01-01','https://cdn.pixabay.com/photo/2016/11/19/15/20/dog-1839808_960_720.jpg',0),
('test@yahoo.com','1234','test','test','12 rue jean jaures',1234123, 'user', '2021-12-01','https://cdn.pixabay.com/photo/2014/06/24/17/34/silhouette-376538_960_720.jpg',1);
