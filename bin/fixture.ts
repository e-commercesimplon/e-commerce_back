import 'dotenv-flow/config';
import { createConnection, getConnection } from 'typeorm';
import { dogFixtures } from '../test/setUp';


async function doFixture() {

    await createConnection({
        type: 'mysql',
        url: process.env.DATABASE_URL,
        synchronize: true,
        entities: ['src/entity/*.ts']
    });
    await getConnection().synchronize(true);
    await dogFixtures()
    
    await getConnection().close();
}

doFixture();
