import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm"
import { User } from "./User";
import Joi from "joi";
import { Accessorie } from "./Accessorie";
import { Dog } from "./Dog";

@Entity()
export class Comment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    text: string;

    @Column({type:'date'})
    date: Date;

    @Column({default:1})
    status:number;

    @Column({default:2.5})
    rating:number;
    
    @ManyToOne(type=>User, user=>user.comments, {eager:true})
    user: User;

    @ManyToOne(type=>Accessorie, accessorie=>accessorie.comments)
    accessorie: Accessorie;
}

const schema = Joi.object({
    text:Joi.string(),
    // date: Joi. ,


    price: Joi.number(),
    description: Joi.string(),
    stock: Joi.number(),
    rating: Joi.number(),
    status: Joi.number(),
})