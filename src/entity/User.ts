import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import { Comment } from "./Comment";
import { Order } from "./Order";
@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password:string;

    @Column()
    name: string;

    @Column()
    firstName: string;
    
    @Column({default:'Unknown'})
    adress: string;

    @Column({default:'0000'})
    creditCard: number;

    @Column({default:'Unknown'})
    role: string;

    @Column({type:'date', default:'1996-11-25'})
    lastConnection: Date;

    @Column({default:'None'})
    avatar: string;

    @Column({default:1})
    status: number;

    @OneToMany(type=>Comment,comment=>comment.user)
    comments:Comment[];

    @OneToMany(type=>Order, order=>order.user)
    orders:Order[];

}
