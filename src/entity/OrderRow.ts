import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm"
import { Accessorie } from "./Accessorie";
import { Article } from "./Article";
import { Dog } from "./Dog";
import { Order } from "./Order";

@Entity()
export class OrderRow {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    price: number;

    @Column()
    quantity: number;

    @ManyToOne(type=>Order, order=>order.orderRows, { cascade: true, onDelete: 'SET NULL' })
    order:Order;

    @ManyToOne(type=>Accessorie, accessorie=>accessorie.orderRows)
    accessorie:Accessorie;

    @ManyToOne(type=>Dog, dog=>dog.orderRows)
    dog:Dog;
}