import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm"
import { Article } from "./Article";
import Joi from "joi";
import { Accessorie } from "./Accessorie";
import { Dog } from "./Dog";


@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default:1})
    status: number;

    @OneToMany(type=>Accessorie, accessorie=>accessorie.category)
    accessories:Accessorie[];

    @OneToMany(type=>Dog, dog=>dog.category)
    dogs:Dog[];
}

const schema = Joi.object({
    name:Joi.string(),
    status: Joi.number()
})