import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm"
import { OrderRow } from "./OrderRow";
import { User } from "./User";

@Entity()
export class Order {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable:true})
    price: number;

    @Column({type:'date', nullable:true})
    date: Date;

    @Column({default:1})
    status:number;
    
    @ManyToOne(type=>User, user=>user.orders)
    user: User;

    @OneToMany(type=>OrderRow,orderRow=>orderRow.order)
    orderRows:OrderRow[]
}