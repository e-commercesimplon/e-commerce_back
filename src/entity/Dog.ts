import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm"
import { Article } from "./Article";
import { Category } from "./Category";
import { Comment } from "./Comment";
import { OrderRow } from "./OrderRow";

@Entity()
export class Dog extends Article {

    @Column()
    breed: string;

    @Column()
    weight: number;

    @Column()
    coat: string;
    
    @ManyToOne(type=>Category, category=>category.dogs)
    category:Category;

    @OneToMany(type=>OrderRow,orderRow=>orderRow.dog)
    orderRows:OrderRow[];
}