import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm"
import { Category } from "./Category";
import { Comment } from "./Comment";
import { OrderRow } from "./OrderRow";
import Joi from "joi";
//ne doit pas être une entity, supprimer et refaire les relations en double dans dog et accessorie

export abstract class Article {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({default:'None'})
    picture: string;

    @Column()
    name: string;

    @Column()
    price: number;

    @Column({default:2.5})
    rating: number;

    @Column({default:1})
    status: number;
}

const schema = Joi.object({
    picture:Joi.string(),
    name: Joi.string(),
    price: Joi.number(),
    description: Joi.string(),
    stock: Joi.number(),
    rating: Joi.number(),
    status: Joi.number(),
})