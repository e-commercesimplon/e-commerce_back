import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm"
import { Article } from "./Article";
import { Category } from "./Category";
import { Comment } from "./Comment";
import { OrderRow } from "./OrderRow";

@Entity()
export class Accessorie extends Article {

    @Column()
    description: string;

    @Column()
    stock: number;
    
    @ManyToOne(type=>Category, category=>category.accessories)
    category:Category;

    @OneToMany(type=>Comment, comment=>comment.accessorie)
    comments:Comment[];

    @OneToMany(type=>OrderRow,orderRow=>orderRow.accessorie)
    orderRows:OrderRow[];
}