import express from 'express';
import cors from 'cors';
import passport from 'passport';
import { getConnection } from 'typeorm';
import { dogFixtures } from '../test/setUp';
import { accountController } from './controller/account-controller';
import { adminController } from './controller/admin-controller';
import { articleController } from './controller/article-controller';
import { categoryController } from './controller/category-controller';
import { commentController } from './controller/comment-controller';
import { userController } from './controller/user-controller';
import { configurePassport, protect } from './utils/token';

configurePassport();

export const server = express();

server.use(passport.initialize());

server.use(express.json())
server.use(express.static('public'));
server.use(cors());

server.use('/api/article', articleController);
server.use('/api/category', categoryController);
server.use('/api/comment', commentController);
server.use('/api/user', userController);
server.use('/api/account', accountController);
server.use('/api/admin',protect(['admin']), adminController);

server.get('/fixtures', async (req, res) => {
    await getConnection().synchronize(true);

    await dogFixtures()
    
    res.end();
})