import fs from 'fs';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';

const privateKey = fs.readFileSync('config/id_rsa');
const publicKey = fs.readFileSync('config/id_rsa.pub');

export function generateToken(payload) {
    const token = jwt.sign(payload, privateKey,{algorithm: 'RS256', expiresIn: 60*60*24*31 });
    return token;
}

export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {
        try {
            const user = await getRepository(User).findOne({ where: { email:payload.email } });
            
            if(user) {
            
                return done(null, user);
            }
            
            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}

export function protect(role = ['any']) {

    return [
        passport.authenticate('jwt', {session: false}),
        (req,res,next) => {
            if(role.includes('any') || role.includes(req.user.role)) {
                next()
            } else {
                res.status(403).json({error: 'Access denied'});
            }
        }
    ]
}
