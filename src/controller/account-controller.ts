import { Router } from "express";
import { User } from "../entity/User";
import { uploader } from "../uploader";
import { getRepository } from "typeorm";
import { UserRequest } from "../utils/UserRequest";
import { Order } from "../entity/Order";
import { protect } from "../utils/token";
import { OrderRow } from "../entity/OrderRow";

export const accountController = Router();

accountController.get('/', protect(), (req:UserRequest, res) => {
    res.json(req.user);
});

accountController.patch('/', protect(), uploader.single('picture'), async (req: UserRequest, res) => {
    try {
        let data = await getRepository(User).findOne(req.user.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = { ...data, ...req.body };
        if (req.file) {
            update.avatar = req.file.filename;
        }
        await getRepository(User).save(update);
        res.status(200).json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

accountController.get('/orders', protect(), async(req:UserRequest,res)=>{
    try {
        let orders = await getRepository(Order).find({where:{status:0, user:req.user}, relations:['orderRows']})

        if (!orders) {
            res.send(404).end();
            return
        }
        
        res.json(orders)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

accountController.get('/cart', protect(), async(req:UserRequest,res)=>{
    try {
        let cart = await getRepository(Order).findOne({status:1, user:req.user}, {relations:['orderRows']})

        if (!cart) {
            res.send(404).end();
            return
        }
        
        res.json(cart)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

accountController.post('/cart', protect(), async(req:UserRequest,res)=>{
    try {
        let newArticle = await getRepository(OrderRow).save(req.body);

        let cart = await getRepository(Order).findOne({status:1, user:req.user}, {relations:['orderRows']})
        let cartPrice = 0;

        if (!cart) {
            const newCart = new Order();
            newCart.user = req.user;
            newCart.orderRows.push(newArticle);
            for (const item of newCart.orderRows) {
                cartPrice+=item.price
            }
            newCart.price=cartPrice;
            await getRepository(Order).save(newCart);
            res.status(201).json(newCart)
            return;
        }
        
        cart.orderRows.push(newArticle);
        for (const item of cart.orderRows) {
            cartPrice+=item.price
        }
        cart.price=cartPrice
        await getRepository(Order).save(cart)
        res.status(201).json(cart)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

accountController.patch('/cart/validate', protect(), async(req:UserRequest,res)=>{
    try {
        let cart = await getRepository(Order).findOne({status:1, user:req.user})

        if (!cart) {
            res.send(404).end();
            return
        }

        cart.status = 0;
        cart.date= new Date();
        await getRepository(Order).save(cart)
        res.status(201).json(cart)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

accountController.delete('/cart', protect(), async(req:UserRequest,res)=>{
    try {
        let cart = await getRepository(Order).findOne({status:1, user:req.user})

        if (!cart) {
            res.send(404).end();
            return
        }

        await getRepository(Order).delete(cart.id)
        res.status(204)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

accountController.delete('/orderRow/:id', protect(), async(req:UserRequest,res)=>{
    try {
        let row = await getRepository(OrderRow).findOne(req.params.id)

        if (!row) {
            res.send(404).end();
            return
        }

        await getRepository(OrderRow).delete(row.id)
        res.status(204)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

accountController.patch('/orderRow/:id', protect(), async(req:UserRequest,res)=>{
    try {
        let row = await getRepository(OrderRow).findOne(req.params.id)

        if (!row) {
            res.send(404).end();
            return
        }
        
        row.quantity=req.body.quantity
        await getRepository(OrderRow).save(row)
        res.status(204)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})