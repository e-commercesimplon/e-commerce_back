import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Category } from "../entity/Category";
import { protect } from "../utils/token";
import { UserRequest } from "../utils/UserRequest";

export const categoryController = Router();

categoryController.get('/', async (req, res) => {
    try {
        const categories = await getRepository(Category).find({relations: ['accessories', 'dogs'], where: {status: 1} })
        res.json(categories)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

// GetByNameCategory
categoryController.get('/:id', async (req, res) => {
    try {
        let category = await getRepository(Category).findOne(req.params.id, { relations: ['accessories', 'dogs'], where: { status: 1 } });
        if(!category) {
            res.status(404).end();
            return;
        }
        res.json(category)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})
