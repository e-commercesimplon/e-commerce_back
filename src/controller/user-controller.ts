import { Router } from "express";
import { User } from "../entity/User";
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import passport from "passport";
import { uploader } from "../uploader";
import { getRepository } from "typeorm";
import { UserRequest } from "../utils/UserRequest";

export const userController = Router();

userController.post('/register', uploader.single('picture'), async (req, res) => {
    try {

        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await getRepository(User).findOne({ where: { email: req.body.email } });
        if (exists) {
            res.status(400).json({ error: 'Email already taken' });
            return;
        }
        if (req.file) {
            newUser.avatar = '/uploads/' + req.file.filename;
        }
        //On assigne user en role pour pas qu'un user puisse choisir son rôle à l'inscription
        newUser.role = 'user';
        //On hash le mdp du user pour pas le stocker en clair
        newUser.password = await bcrypt.hash(newUser.password, 11);

        newUser.status = 1;

        await getRepository(User).save(newUser);

        res.status(201).json({
            user: newUser,
            token: generateToken({
                email: newUser.email,
                id: newUser.id,
                role: newUser.role
            })
        });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userController.post('/login', async (req, res) => {
    try {
        const user = await getRepository(User).findOne({ where: { email: req.body.email } });
        if (user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if (samePassword) {
                if (user.status === 1) {
                    user.lastConnection = new Date();
                    await getRepository(User).save(user);
                    res.json({
                        user,
                        token: generateToken({
                            email: user.email,
                            id: user.id,
                            role: user.role
                        })
                    });
                    return;
                }
                res.status(201).json({error: 'Disabled account'});
                return;
            }
        }
        res.status(401).json({ error: 'Wrong email and/or password' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.delete('/:id', protect(), async (req: UserRequest, res) => {
    try {
        let toDelete = await getRepository(User).findOne(req.params.id)
        if (!toDelete) {
            res.status(404);
            return
        }
        
        if (req.user.id === toDelete.id || req.user.role === 'admin') {
            toDelete.status = 0
            await getRepository(User).save(toDelete)
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

userController.get('/account', protect(), (req:UserRequest, res) => {
    res.json(req.user);
});