import { Router } from "express";
import Joi from "joi";
import { getRepository } from "typeorm";
import { Accessorie } from "../entity/Accessorie";
import { Article } from "../entity/Article";
import { Dog } from "../entity/Dog";
import { uploader } from "../uploader";
import { protect } from "../utils/token";
import { UserRequest } from "../utils/UserRequest";

export const articleController = Router();

articleController.get('/:article', async (req, res) => {
    try {
        let articles: Article[];
        let entity: typeof Article;
        if (req.params.article === 'accessorie') {
            entity = Accessorie
        } else if (req.params.article === 'dog') {
            entity = Dog
        } else {
            res.status(400).end();
            return;
        }

        if (req.query.search) {
            articles = await getRepository(entity)
                .createQueryBuilder(entity.name.toLowerCase())
                .where('name like :search', { search: `%${req.query.search}%` })
                .andWhere('status = 1')
                .getMany()
            // .relation('category').loadMany()
        } else {
            articles = await getRepository(entity).find({ relations: ['category'], where: { status: 1 } });
        }
        res.status(200).json(articles);
        return;

    } catch (error) {
        console.log(error);
        res.status(500).json(error)


    }
});

articleController.get('/:article/:id', async (req: UserRequest, res) => {
    try {
        let article: Article;
        let entity: typeof Article;
        if (req.params.article === 'accessorie') {
            entity = Accessorie
        } else if (req.params.article === 'dog') {
            entity = Dog
        } else {
            res.status(400).end();
            return;
        }
        article = await getRepository(entity).findOne(req.params.id, { relations: entity === Accessorie ? ['category', 'comments'] : ['category'], where: { status: 1 } });


        res.status(200).json(article);
        return;
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
});
