import { Router } from "express";
import { User } from "../entity/User";
import { uploader } from "../uploader";
import { getRepository } from "typeorm";
import { UserRequest } from "../utils/UserRequest";
import { Dog } from "../entity/Dog";
import { Accessorie } from "../entity/Accessorie";
import { Category } from "../entity/Category";
import { Article } from "../entity/Article";

export const adminController = Router();

adminController.get('/user/:id', async (req, res) => {
    try {
        let user = await getRepository(User).findOne(req.params.id, { relations: ['orders'] })

        if (!user) {
            res.send(404).end();
            return
        }

        res.json(user)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

adminController.post('/article/:article', uploader.single('picture'), async (req: UserRequest, res) => {
    try {
        let article: Article;
        let entity: typeof Article;
        if (req.params.article === 'accessorie') {
            entity = Accessorie;
            article = new Accessorie;
        } else if (req.params.article === 'dog') {
            entity = Dog;
            article= new Dog;
        } else {
            res.status(401).end();
            return;
        }
        
        Object.assign(article, req.body);
        
        if (req.user.role === 'admin') {            
            article.status = 1;
            await getRepository(entity).save(article);
            res.status(201).json(article);
            return;
        }
        res.status(401).end();
        return;

    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
});



//         const schema = Joi.object({
//             name: Joi.string().uppercase(),
//             price: Joi.number().min(0.1).required(),
//             rating: Joi.number().min(1).max(5).required()

//         const { error } = schema.validate(req.body, { abortEarly: false });


adminController.patch('/article/:article/:id', uploader.single('picture'), async (req: UserRequest, res) => {
    try {

        let article: Article;
        let entity: typeof Article;
        if (req.params.article === 'accessorie') {
            entity = Accessorie
        } else if (req.params.article === 'dog') {
            entity = Dog
        } else {
            res.status(401).end();
            return;
        }

        article = await getRepository(entity).findOne(req.params.id);
        if (!article) {
            res.status(404).end();
            return
        }
        Object.assign(article, req.body);
        if(req.file){
            article.picture = '/uploads/' + req.file.filename;
        }
        if (req.user.role === 'admin') {
            getRepository(entity).save(article);
            res.status(200).json(article);
            return;
        }
        res.status(401).end();

    } catch (error) {
        console.log(error)
        res.status(500).json(error)
    }
});

adminController.delete('/article/:article/:id', async (req: UserRequest, res) => {
    try {
        let entity: typeof Article;
        if (req.params.article === 'accessorie') {
            entity = Accessorie
        } else if (req.params.article === 'dog') {
            entity = Dog
        } else {
            res.status(401).end();
            return;
        }

        let toDelete = await getRepository(entity).findOne(req.params.id)
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.role === 'admin') {
            toDelete.status = 0
            await getRepository(entity).save(toDelete)
            res.status(204).end();
            return;
        }
        res.status(401).end();

    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

adminController.post('/category', async (req:UserRequest, res) => {
    try {
        let category = new Category();
        Object.assign(category, req.body)
        if (req.user.role === 'admin') {
            await getRepository(Category).save(req.body)
            res.status(201).json(category)
            return;
        }
        res.status(401).end();
        return;
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

adminController.patch('/category/:id', async (req:UserRequest, res) => {
    try {
        const category = await getRepository(Category).findOne(req.params.id);
        if (!category) {
            res.status(404).end();
            return
        }
        Object.assign(category, req.body);
        if (req.user.role === 'admin') {
            getRepository(Category).save(category);
            res.json(category);
            return;
        }
        res.status(401).end();

    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

adminController.delete('/category/:id', async (req: UserRequest, res) => {
    try {
        let toDelete = await getRepository(Category).findOne(req.params.id)
        if (!toDelete) {
            res.status(404)
            return
        }
        if (req.user.role === 'admin') {
            toDelete.status =0;
            await getRepository(Category).save(toDelete)
            res.status(204).end();
            return;
        }
        res.status(401).end();
        return;
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})
