import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Accessorie } from "../entity/Accessorie";
import { Comment } from "../entity/Comment";
import { protect } from "../utils/token";
import { UserRequest } from "../utils/UserRequest";

export const commentController = Router();

commentController.get('/', async (req, resp) => {
    let comment = await getRepository(Comment).find({ where: { status: 1 } });
    resp.json(comment)
})

commentController.get('/:id', async (req, resp) => {
    let repo = getRepository(Comment)
    let comment = await repo.findOne(req.params.id, { where: { status: 1 } });
    if (!comment) {
        resp.status(404).json({ error: 'Pas trouvé' });
        return;
    }
    resp.json(comment)
}) 

commentController.post('/:accessorieId', protect(), async (req: UserRequest, resp) => {
    try {
        let repo = getRepository(Comment)
        const newComment = new Comment();
        Object.assign(newComment, req.body);
        console.log(req.body);
        
        newComment.user = req.user;
        newComment.date = new Date();
        let accessorie = await getRepository(Accessorie).findOne(req.params.accessorieId)
        newComment.accessorie = accessorie;
        await repo.save(newComment);
        resp.status(201).json(req.body)
    } catch (error) {
        console.log(error)
    }

})

commentController.patch('/:id', protect(), async (req: UserRequest, resp) => {
    try {
        let repo = getRepository(Comment)
        let toUpdate = await repo.findOne(req.params.id, { relations: ['user'] });
        if (!toUpdate) {
            resp.status(404).end()
            return
        }

        if (req.user.id === toUpdate.user.id) {
            toUpdate.text = req.body.text
            toUpdate.date = new Date()
            await repo.save(toUpdate)
            resp.status(200).json(toUpdate)
        }
        else {
            resp.status(401).end()
        }
    } catch (error) {
        console.log(error)
        resp.status(500).end()
    }

})

commentController.delete('/:id', protect(), async (req: UserRequest, resp) => {
    try {
        let repo = getRepository(Comment)
        let toDelete = await repo.findOne(req.params.id, { relations: ['user'] })
        if (!toDelete) {
            resp.status(404).end()
            return
        }
        if (req.user.id === toDelete.user.id || req.user.role === 'admin') {
            toDelete.status = 0
            await repo.save(toDelete)
            resp.status(201).end()
        }
    } catch (error) {
        console.log(error)
        resp.status(500).end
    }
})
